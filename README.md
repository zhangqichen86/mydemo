# C/C++语言编写的代码demo

## 仓库内容

1. CopyTool 文件夹：简易的文件传输工具demo。
2. SQLitedemo 文件夹: 简易的SQL语句使用，对数据库的操作。
3. SocketDemo 文件夹：TCP/UDP协议实现的简易聊天以及文件传输功能。
4. cppdemo 文件夹：简单的cpp编写代码。

## QtCreator编写的demo

1. myMusicPlayer文件夹: 基于qt库的简单音乐播放器demo。


### 仓库用途

主要用于存储代码编写过程中实现的小功能，已经可复用的demo代码作为后续参考。


# 努力成为一名优秀的IT工程师！！！
