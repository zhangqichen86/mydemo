#ifndef MYPLAYER_H
#define MYPLAYER_H

#include <QWidget>
#include <QMainWindow>
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QtMultimedia>
#include <QtMultimediaWidgets>



QT_BEGIN_NAMESPACE
namespace Ui { class myPlayer; }
QT_END_NAMESPACE

class myPlayer : public QWidget
{
    Q_OBJECT

public:
    myPlayer(QWidget *parent = nullptr);
    ~myPlayer();

private slots:
    void on_btnPlayPause_clicked();
    void on_btnMute_clicked();
    void ChangeMuted();
    void PlayPauseMusic();
    void LoopStyle();
    void on_sliVoiceSound_sliderMoved(int position);
    void on_sliPosition_sliderMoved(int position);
    void updatePosition(qint64 position);

private:
    Ui::myPlayer *ui;
    QMediaPlayer* m_player;
    QVideoWidget* m_videoWidget;
    QMediaPlaylist* m_playlist;
};
#endif // MYPLAYER_H
