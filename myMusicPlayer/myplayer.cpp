#include "myplayer.h"
#include "ui_myplayer.h"
#include <QString>
#include <QDateTime>
#include <QMediaPlayer>
#include <QtMultimedia>
#include <QMediaContent>
#include <QMediaMetaData>



QString convertTime(qint64 duration);

myPlayer::myPlayer(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::myPlayer)
{
    ui->setupUi(this);
    m_player = new QMediaPlayer(this);
    m_videoWidget = new QVideoWidget(this);
    m_playlist = new QMediaPlaylist(this);

    connect(ui->btnPlayPause, SIGNAL(clicked()), this, SLOT(PlayPauseMusic()));
    connect(ui->btnMute, SIGNAL(clicked()), this, SLOT(ChangeMuted()));
    connect(ui->btnLoop, SIGNAL(clicked()), this, SLOT(LoopStyle()));

    connect(m_player, &QMediaPlayer::positionChanged, this, &myPlayer::updatePosition);

    m_player->setMedia(QMediaContent(QUrl::fromLocalFile("/home/zhangqichen/music/file.mp3")));
    m_playlist->setPlaybackMode(QMediaPlaylist::Sequential);

    m_player->setVolume(50);
    ui->sliVoiceSound->setValue(50);
    ui->labelVolume->setText(QString("%1%").arg(50));

    ui->sliPosition->setValue(0);
    ui->sliPosition->setMinimum(0);

    ui->edtName->setText("file");
}


myPlayer::~myPlayer()
{
    delete ui;
}


void myPlayer::on_btnPlayPause_clicked()
{

}

void myPlayer::on_btnMute_clicked()
{

}



void myPlayer::PlayPauseMusic()
{
    static int state = 1;
    if(state == 1)
    {
        m_player->play();
        ui->btnPlayPause->setText("暂停");
        state = 2;
    }
    else if(state == 2)
    {
        m_player->pause();
        ui->btnPlayPause->setText("播放");
        state = 1;
    }
}

void myPlayer::ChangeMuted()
{
    static int is_mute = 1;
    if(is_mute == 1)
    {
        m_player->setMuted(true);
        ui->btnMute->setText("取消静音");
        is_mute = 2;
    }
    else if(is_mute == 2)
    {
        m_player->setMuted(false);
        ui->btnMute->setText("静音");
        is_mute = 1;
    }
}

void myPlayer::LoopStyle()
{
    static int loop = 1;
    if(loop == 1)
    {
        m_playlist->setPlaybackMode(QMediaPlaylist::Random);
        ui->btnLoop->setText("随机播放");
        loop = 2;
    }
    else if(loop == 2)
    {
        m_playlist->setPlaybackMode(QMediaPlaylist::CurrentItemInLoop);
        ui->btnLoop->setText("单曲循环");
        loop = 3;
    }
    else if(loop == 3)
    {
        m_playlist->setPlaybackMode(QMediaPlaylist::Loop);
        ui->btnLoop->setText("列表循环");
        loop = 1;
    }
}

//  音量槽函数
void myPlayer::on_sliVoiceSound_sliderMoved(int position)
{
    m_player->setVolume(position);
    ui->sliVoiceSound->setValue(position);
    ui->labelVolume->setText(QString("%1%").arg(position));
}

//  位置更新
void myPlayer::updatePosition(qint64 position)
{
    ui->sliPosition->setValue(position);
    QString currentTime = convertTime(position);
    QString endTime = convertTime(m_player->duration());
    QString nowTime = currentTime + '/' + endTime;

    ui->labelTime->setText(nowTime);
}

QString convertTime(qint64 durationMs){

  int seconds = durationMs / 1000;
  int minutes = seconds / 60;
  seconds %= 60;

  return QString("%1:%2").arg(minutes,2,10,QChar('0'))
                         .arg(seconds,2,10,QChar('0'));

}

void myPlayer::on_sliPosition_sliderMoved(int position)
{
    m_player->setPosition(position);
    qint64 duration = m_player->duration();
    ui->sliPosition->setMaximum(duration);
}
