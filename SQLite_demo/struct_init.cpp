#include <iostream>
#include <string>
#include <string.h>
#include <stdbool.h>


using namespace std;

typedef struct wanna
{
    int id;
    char name[20];
    float sight;
    string phone;
    double score;
    bool is_usefully;  

} wanna;

void init_wanna(wanna& i_wanna);

int main()
{
    wanna myself = {101, "张三", 5.0, "15927636782", 80.8, true};
    // cout << "请输入ID：" << flush;
    // cin >> myself.id;
    // cout << "请输入姓名：" << flush;
    // cin >> myself.name;
    // cout << "请输入视力：" << flush;
    // cin >> myself.sight;
    // cout << "请输入电话：" << flush;
    // cin >> myself.phone;
    // cout << "请输入成绩：" << flush;
    // cin >> myself.score;

    // myself.is_usefully = true;
    // cout << endl;

    cout << "序号: " << myself.id << endl;
    cout << "姓名: " << myself.name << endl;
    cout << "视力: " << myself.sight << endl;
    cout << "手机号: " << myself.phone << endl;
    cout << "分数: " << myself.score << endl;
    cout << "是否有效:（1.有效  0.无效） " << myself.is_usefully << endl << endl;  
    
    init_wanna(myself);

    cout << "初始化后的结果如下:" << endl;
    cout << "序号: " << myself.id << endl;
    cout << "姓名: " << myself.name << endl;
    cout << "视力: " << myself.sight << endl;
    cout << "手机号: " << myself.phone << endl;
    cout << "分数: " << myself.score << endl;
    cout << "是否有效:（1.有效  0.无效） " << myself.is_usefully << endl << endl; 

    return 0;
}


void init_wanna(wanna& i_wanna)
{
    i_wanna.id = 0;
    strcpy(i_wanna.name, "\0");
    i_wanna.sight = 0;
    i_wanna.phone = "";
    i_wanna.score = 0;
    i_wanna.is_usefully = false;

    return ;
}