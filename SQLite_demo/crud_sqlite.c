
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include <unistd.h>


int main()
{
    sqlite3* db = NULL;
    int len;
    char* err_msg = NULL;

    // 打开 sqlite3 库
    len = sqlite3_open("mydata.db",&db);
    if(len)
    {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        exit(EXIT_FAILURE);
    }

    printf("open sqlite3 successfully\n");

    #ifdef _DEBUG_
        printf("%s\n", err_msg);
    #endif

    int sno;
    char name[21], sex[4], phone[12];
    float sight;
    char sql[500];
    
    // 增加数据
    printf("\n请按照提示输入学生各项信息\n");
    printf("\n学号:");
    scanf("%d", &sno);
    printf("\n姓名:");
    scanf("%s", name);
    printf("\n性别:");
    scanf("%s", sex);  
    printf("\n手机号:");
    scanf("%s", phone);
    printf("\n视力:");
    scanf("%f", &sight);

    sprintf(sql, "INSERT INTO student VALUES (%d,'%s','%s','%s',%g);", sno,name,sex,phone,sight);
 
    if(!sqlite3_exec(db, sql, NULL, NULL, &err_msg))
    {
        printf("\n录入成功\n");
    }
    else
    {
        fprintf(stderr, "\n录入失败！(原因：%s)\n", err_msg);
    }

    printf("\n请输入要删除学生的学号：");
    scanf("%d", &sno);

    sprintf(sql, "DELETE FROM student WHERE sno = %d;", sno);
 
    if(!sqlite3_exec(db, sql, NULL, NULL, &err_msg))
    {
        if(sqlite3_changes(db))
            printf("\n删除成功！\n");
        else
            printf("\n学号:(%d)不存在！\n", sno);
    }
    else
    {
        fprintf(stderr, "\n删除失败！(原因：%s)\n", err_msg);
    }
    
    // 单表查找数据
    char name1[30];
    int row = 0, col = 0;
    char ** res_data = NULL;
    printf("\n请输入要查找学生的名字：");
    scanf("%s", name1);
    
    sprintf(sql, "SELECT * FROM student WHERE name LIKE '%s%%';", name1);
    
    if(sqlite3_get_table(db, sql, &res_data, &row, &col, &err_msg))
    {
        fprintf(stderr, "\n查询失败！（原因：%s）\n", err_msg);
    }
    else
    {
        if(row >0)
        {

            printf("\n共查询到%d名学生信息，查询到的结果如下:\n", row);
            for(int i = 1; i < row + 1; i++)
            {
                for(int j = 0; j < col; j++)
                {
                    printf("%s ", res_data[i * col + j]);
                }
                printf("\n");
            }
        }
        else if(row == 0)
            printf("\n没有查询到任何符合条件的信息！\n");
    }  

    // 多表查找数据  

    sprintf(sql, "SELECT AVG(score) FROM student s, course c, score sc WHERE s.sno = sc.sno and c.cno = sc.cno and c.name = '语文' and sex = '男';");

    int row1 = 0, col1 = 0;
    char** result = NULL;
    int i, j;

    if(sqlite3_get_table(db, sql, &result, &row1, &col1, &err_msg))
    {
        fprintf(stderr, "\n查询失败！（原因：%s）\n", err_msg);
    }
    else
    {
        if(row1 == 0)
        {
            printf("\n没有查询到任何符合条件的记录！\n");
        }
        else if(row1 > 0)
        {
            printf("\n班上所有男生的《语文》这门课的平均分数为：%d 分\n", atoi(result[1]));
        }
    }

    // 修改数据

    int wanna_sno = 0, ch = 0;
    printf("\n请输入要修改学生的学号:");
    scanf("%d%*c", &wanna_sno);

    printf("\n请选择要修改的类型:1.手机号 2.视力：");
    scanf("%d%*c", &ch);
    float new_sight;
    char new_phone[12];
    switch(ch)
    {
        case 1:
            
            printf("请输入新的手机号：");
            scanf("%s", new_phone);
            sprintf(sql, "UPDATE student SET phone = '%s'", new_phone);
            break;
        case 2:
            
            printf("请输入新的视力值：");
            scanf("%f", &new_sight);
            sprintf(sql, "UPDATE student SET phone = %f", new_sight);
            break;
        default:
            printf("选择错误，请重试！\n");
    }

    sqlite3_free_table(res_data);


    int row2 = 0, col2 = 0;
    char** result1 = NULL;
    int m, n;

    sprintf(sql, "SELECT * FROM student;");

    if(sqlite3_get_table(db, sql, &res_data, &row, &col, &err_msg))
    {
        fprintf(stderr, "\n查询失败！（原因：%s）\n", err_msg);
    }
    else
    {
        if(row >0)
        {

            printf("\n共查询到%d名学生信息，查询到的结果如下:\n", row);
            for(int m = 1; m < row + 1; m++)
            {
                for(int n = 0; n < col; n++)
                {
                    printf("%s ", res_data[m * col + n]);
                }
                printf("\n");
            }
        }
        else if(row == 0)
            printf("\n没有查询到任何符合条件的信息！\n");
    }  
    // 关闭 sqlite3 库
    sqlite3_close(db);

    return 0;
}