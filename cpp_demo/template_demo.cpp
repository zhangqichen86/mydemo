#include <iostream>

using namespace std;

class mylist
{
private:
    // 类型成员
    struct node
    {
        double data;
        node* next;
    };
    // 数据成员
    node* head;
    node* tail;
    int len;

public:
    // 函数成员
    mylist();   // 创建链表
    mylist(const mylist& l);
    ~mylist();  // 销毁链表

    void push_back(const double& data);
    void push_front(const double& data);
    bool remove(const double& data);
    double& find(const double& data);
    void traverse(bool(*visit)(double& data));
    void sort(bool(*cmp)(const double&, const double&));
    void reverse(void);
    void clear(void);

    int size(void) const;
    bool empty(void) const;


    //mylist operator=(const mylist& l);
};

bool show(double& data);
bool cmp(const double& a, const double& b);

int main()
{
    mylist l1, l2;
    l1.push_back(3.14);
    l1.push_back(2.22);
    l1.push_front(-1.11);
    l1.push_front(-2.22);
    l1.push_front(6.66);
    l1.push_front(-7.77);
    l1.push_front(9.99);
    l1.traverse(show);

    l1.sort(cmp);
    l1.traverse(show);
    l1.reverse();
    l1.traverse(show);

    std::cout << l1.size() << std::endl;
    std::cout << l2.size() << std::endl;



    return 0;
}

mylist::mylist()
{
    mylist::node* n = new mylist::node;
    n->data = 0;
    n->next = nullptr;
    head = n;
    tail = n;
    len = 0; 
}

mylist::mylist(const mylist& l)
{

}

mylist::~mylist()
{
    node*p = nullptr;

    while(head != nullptr)
    {
        p = head->next;
        delete head;
        head = p;
    }
}

void mylist::clear(void)
{
    node* p = head->next, *q;
    head->next = nullptr;
    
    while(p != nullptr)
    {
        q = p->next;
        delete p;
        p = q;
    } 

    tail = head; 
    len = 0;  
}



void mylist::push_back(const double& data)
{
    node* n = new node;
    n->data = data;
    n->next = nullptr;

    tail->next = n;
    tail = n;
    len++;
}

void mylist::push_front(const double& data)
{
    node* n = new node;
    n->data = data;
    n->next = head->next;
    head->next = n;

    if(n->next == nullptr) tail = n;

    len++;
}

bool mylist::remove(const double& data)
{
    node* p = head, *q;
    while(p->next != nullptr && p->next->data != data)
    {
        p = p->next;
    }
    if(p->next == nullptr)
        return false;
    q = p->next;
    p->next = q->next;
    delete q;
    
    if(nullptr == p->next) tail = p;
    
    len--;
    return true;
}

double& mylist::find(const double& data)
{
    node* p = head->next;

    // 遍历链表找到要修改的节点
    while(p != nullptr && p->data != data)
        p = p->next;

    // 如果没找到目标节点
    if(p == nullptr) throw 1;

    // 如果找到了目标节点就返回其数据域的指针
    return p->data;   
}

void mylist::traverse(bool(*visit)(double& data))
{
    node* p = head->next;  // 跳过头节点

    // 遍历链表所有节点
    while(p != nullptr)
    {
        if(!visit(p->data))
            break;

        p = p->next;
    }    
    std::cout << std::endl;
}

void mylist::reverse(void)
{
    node* p = nullptr, *q = nullptr;

    if(len <= 1) return;

    p = head->next->next;
    head->next->next = nullptr;
    tail = head->next;

    while(p != nullptr)
    {
        q = p->next;
        p->next = head->next;
        head->next = p;

        p = q;
    }
}

void mylist::sort(bool(*cmp)(const double&, const double&))
{
    node* p = nullptr, *q = nullptr, *k = nullptr;
    double tmp;

    p = head->next;

    if(p == nullptr) return;

    while(p->next != nullptr)
    {
        q = p->next;
        k = p;

        while(q != nullptr)
        {
            if(cmp(q->data, k->data) == false)
                k = q;

            q = q->next;
        }

        if(k != p)
        {
            // 交换两个节点的数据域
            tmp = k->data;
            k->data = p->data;
            p->data = tmp;
        }

        p = p->next;
    }
}

int mylist::size(void) const
{
    return len;
}

bool mylist::empty(void) const
{
    return !len;
}

bool show(double& data)
{
    std::cout << data << ' ' << flush;
    return true;
}


bool cmp(const double& a, const double& b)
{
    return a > b;
}
