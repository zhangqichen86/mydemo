#include <iostream>
#include <string>

int divide(int a, int b);
void calc(void);

class my_exception
{
private:
    int err_code;
    std::string err_msg;

public:
    my_exception(int err_code, std::string err_msg);
    //int get_err_code();     //常方法可以重载
    int get_err_code() const;
    std::string get_err_msg() const;
};


int main()
{
    try
    {
        calc();
    }
    // catch(...)  // ... 用来接受任意的异常值
    // {

    // }
    catch(const my_exception& e)
    {
        if(e.get_err_code() == 1)
        {

        }
        std::cerr << e.get_err_msg() << std::endl;
    }

    return 0;
}

my_exception::my_exception(int err_code, std::string err_msg) : err_code(err_code), err_msg("")
{
    this->err_code = err_code;
    this->err_msg = err_msg;
}

int my_exception::get_err_code() const
{
    return err_code;
}

std::string my_exception::get_err_msg() const
{
    return err_msg;
}


void calc(void)
{
    int m, n;
    std::cin >> m;
    std::cin >> n;
    try
    {
        int r = divide(m, n);
        std::cout << r << std::endl;
    }
    catch(int& err)
    {
        if(err == 1)
            std::cout << "被除数不能为0" << std::endl;
    }
 
    return ;
}


int divide(int a, int b)
{
    //     C语言风格
    //     if(b == 0)  
    //     {
    //         perror("divide falied");
    //         return -1;
    //     }
    
    // C++风格的异常捕获
    // try
    // {
    //     /* code */
    // }
    // catch(const std::exception& e)
    // {
    //     std::cerr << e.what() << '\n';
    // }
    
    try
    {
        if(b == 0) 
           throw my_exception(1, "被除数为0");
           //throw std::runtime_error("divid error");  // 使用 throw 关键字抛出异常信息
        std::cout << "test1" << std::endl;
    }
    catch(double) // 1个try块可以配合多个catch，不能多对一
    {
        ;
    }
    catch(char)
    {
        std::cout << "被除数不能为0" << std::endl;
        return 1;
    }    
    
    return (a - b) / b;
}