#include <iostream>

using namespace std;

class A
{
public:
    int i;

    A()
    {
        i = 3;
    }

    void f()
    {

    }

    void F() const;
};



int main()
{
    const A a;
    const A& b = a;
    const A* c = &a;

    cout << a.i + 3 << endl;
    //a.f()    常对象不能调用非常方法
    a.F();
    b.F();
    c->F();

    *((int*)&a) = 5;

    return 0;
}

// 常方法中，所有的属性都是只读的 这里的只读并非指的属性存储到常量区中 而是操作限制读写
void A::F() const   //定义 和 声明分开时 后面要加const
{
    cout << "我是常方法" << endl;
}