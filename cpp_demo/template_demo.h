#include <iostream>

using namespace std;

class mylist
{
private:
    // 类型成员
    struct node
    {
        double data;
        node* next;
    };
    // 数据成员
    node* head;
    node* tail;
    int len;

public:
    // 函数成员
    mylist();   // 创建链表
    mylist(const mylist& l);
    ~mylist();  // 销毁链表

    void push_back(const double& data);
    void push_front(const double& data);
    bool remove(const double& data);
    double& find(const double& data);
    void traverse(bool(*visit)(double& data));
    void sort(bool(*cmp)(const double&, const double&));
    void reverse(void);
    void clear(void);

    int size(void) const;
    bool empty(void) const;


    //mylist operator=(const mylist& l);
};

bool show(double& data);
bool cmp(const double& a, const double& b);


