#include<iostream>
using namespace std;

int g = 9;

void f()
{
    cout << "::show()" << endl;
}


namespace junge
{
    int g = 3;

    void f()
    {
        cout << "junge::show()" << endl;
    }
}

namespace dongge
{
    int g = 3;

    void f()
    {
        cout << "dongge::show()" << endl;
    }
}

int main()
{   
    junge::g = 4;
    junge::f(); 
    cout << junge::g << endl;

    
    dongge::g = 6;
    dongge::f();
    cout << dongge::g << endl;

    cout << ++g << endl;
    f();

    return 0;
}
