#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>    
#include <sys/stat.h>
#include <fcntl.h>   
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>


// 客户端信息结构体声明
typedef struct 
{
    int sock;
    char ip[16];
    unsigned short port;

} client_info;


const char* send_file_path = NULL;
char file_name[100] = "";


void* comm_thr(void* arg);


int main(int argc, char** argv)
{
    const char* p = NULL;

    if(argc != 2)
    {
        fprintf(stderr, "Usage: %s send_file\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if(-1 == access(argv[1], R_OK))
    {
        fprintf(stderr, "要发送的文件(%s)不存在或不可读！\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    send_file_path = argv[1];

    p = strrchr(send_file_path, '/');

    if(p != NULL) p++;
    else p = send_file_path;

    strcpy(file_name, p);


    signal(SIGPIPE, SIG_IGN);

    // 第 1 步：创建监听套接字  
    int sock_listen = socket(AF_INET, SOCK_STREAM, 0);

    if(sock_listen == -1)
    {
        perror("socket error");
        exit(EXIT_FAILURE);
    }

    // 设置套接字的 SO_REUSEADDR 属性为 1，以开启地址复用
    int val = 1;
    setsockopt(sock_listen, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));

    // 第 2 步：绑定
    struct sockaddr_in myaddr;            // 通过 myaddr 结构体指定要绑定的地址
    myaddr.sin_family = AF_INET;          // 指定地址家族为 AF_INET
    //myaddr.sin_addr.s_addr = inet_addr("192.168.0.118");  // 指定 IP 地址为本机的某个特定 IP 地址
    myaddr.sin_addr.s_addr = INADDR_ANY;  // 指定 IP 地址为本机任意 IP 地址（0.0.0.0）
    myaddr.sin_port = htons(8888);          // 指定端口号为 8888

    if(-1 == bind(sock_listen, (struct sockaddr*)&myaddr, sizeof(myaddr)))
    {
        perror("bind error");
        exit(EXIT_FAILURE);
    }

    // 第 3 步：监听
    if(-1 == listen(sock_listen, 5))  // 第 2 个参数表示监听等待队列的长度
    {
        perror("listen error");
        exit(EXIT_FAILURE);
    }

    pthread_t tid;
    client_info* ci = NULL;

    while(1)
    {
        // 第 4 步：接受客户端连接请求
        //int sock_conn = accept(sock_listen, NULL, NULL);  // 如果对当前客户端地址不感兴趣，第 2、3 参数直接传 NULL

        struct sockaddr_in client_addr;
        socklen_t addr_len = sizeof(client_addr);
        // 如果对客户端地址感兴趣，就需要使用 accept 函数的后两个参数
        int sock_conn = accept(sock_listen, (struct sockaddr*)&client_addr, &addr_len);  // 如果对当前客户端地址不感兴趣，第 2、3 参数直接传 NULL
        int ret;

        if(-1 == sock_conn)
        {
            perror("accept error");
            exit(EXIT_FAILURE);
        }

        ci = malloc(sizeof(client_info));

        if(ci == NULL)
        {
            perror("malloc error");
            close(sock_conn);
            continue;
        }

        ci->sock = sock_conn;
        strcpy(ci->ip, inet_ntoa(client_addr.sin_addr));
        ci->port = ntohs(client_addr.sin_port);

        // 为每个客户端创建一个单独的线程，该线程专门负责与该客户端通信
        if(pthread_create(&tid, NULL, comm_thr, ci))
        {
            perror("pthread_create error");
            close(sock_conn);
            free(ci);
            continue;
        }
    }

    // 第 7 步：关闭监听套接字
    close(sock_listen);

    return 0;
}


// 负责与客户端通信的线程
void* comm_thr(void* arg)
{
    client_info* ci = (client_info*)arg;
    char buff[1024];
    int ret;
    int fd;

    pthread_detach(pthread_self());

    printf("客户端(%s:%d)已上线...\n", ci->ip, ci->port);

    // 第 5 步：收发数据

    // 先发送文件名（定长 100 Bytes）
    if(sizeof(file_name) != send(ci->sock, file_name, sizeof(file_name), 0))
    {
        close(ci->sock);
        printf("客户端(%s:%d)已下线！\n", ci->ip, ci->port);
        free(ci);
        return NULL;
    }

    // 读文件数据并发送
    fd = open(send_file_path, O_RDONLY);

    while((ret = read(fd, buff, sizeof(buff))) > 0)
    {
        if(write(ci->sock, buff, ret) != ret) break;
    }

    close(fd);

    // 第 6 步：断开连接
    close(ci->sock);

    printf("客户端(%s:%d)已下线！\n", ci->ip, ci->port);

    free(ci);

    return NULL;
}