#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>


int main(int argc, char** argv)
{
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd == -1)
    {
        perror("socket error");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in myaddr;

    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = INADDR_ANY;
    myaddr.sin_port = htons(8888);

    if(-1 == bind(sockfd, (struct sockaddr*)&myaddr, sizeof(myaddr)))
    {
        perror("bind error");
        exit(EXIT_FAILURE);
    }

    int tmp = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &tmp, sizeof(tmp));

    struct sockaddr_in target_addr;
    target_addr.sin_family = AF_INET;
    target_addr.sin_addr.s_addr = inet_addr("192.168.0.255");
    target_addr.sin_port = htons(2425);

    char msg[500] = "1:1000:66:huohuohuo:32:我是你张哥";
    int ret;

    sendto(sockfd, msg, strlen(msg), 0, (struct sockaddr*)&target_addr, sizeof(target_addr));

    close(sockfd);

    return 0;
}