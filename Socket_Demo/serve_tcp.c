#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>       
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>


int main()
{
    // 第 1 步：创建监听套接字  
    int sock_listen = socket(AF_INET, SOCK_STREAM, 0);

    if(sock_listen == -1)
    {
        perror("socket error");
        exit(EXIT_FAILURE);
    }


    // 第 2 步：绑定
    struct sockaddr_in myaddr;            // 通过 myaddr 结构体指定要绑定的地址
    myaddr.sin_family = AF_INET;          // 指定地址家族为 AF_INET
    //myaddr.sin_addr.s_addr = inet_addr("192.168.0.118");  // 指定 IP 地址为本机的某个特定 IP 地址
    myaddr.sin_addr.s_addr = INADDR_ANY;  // 指定 IP 地址为本机任意 IP 地址（0.0.0.0）
    myaddr.sin_port = htons(88);          // 指定端口号为 88

    if(-1 == bind(sock_listen, (struct sockaddr*)&myaddr, sizeof(myaddr)))
    {
        perror("bind error");
        exit(EXIT_FAILURE);
    }


    // 第 3 步：监听
    if(-1 == listen(sock_listen, 5))  // 第 2 个参数表示监听等待队列的长度
    {
        perror("listen error");
        exit(EXIT_FAILURE);
    }


    while(1)
    {
        // 第 4 步：接受客户端连接请求
        int sock_conn = accept(sock_listen, NULL, NULL);  // 如果对当前客户端地址不感兴趣，第 2、3 参数直接传 NULL

        if(-1 == sock_conn)
        {
            perror("accept error");
            exit(EXIT_FAILURE);
        }

        
        // 第 5 步：收发数据
        char msg[] = "你好，客户端！";
        double d;

        send(sock_conn, msg, strlen(msg), 0);  // 发送数据

        recv(sock_conn, &d, sizeof(d), 0);     // 接收数据
        printf("客户端说：%g\n", d);

        // 第 6 步：断开连接
        close(sock_conn);
    }

    // 第 7 步：关闭监听套接字
    close(sock_listen);

    return 0;
}