#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>    
#include <sys/stat.h>
#include <fcntl.h>   
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>


// 客户端信息结构体声明
typedef struct 
{
    int sock;
    char ip[16];
    unsigned short port;

} client_info;


void* comm_thr(void* arg);


int main(int argc, char** argv)
{
    int fd;
    char buff[1025] = "";
    char file_name[100] = "";

    if(argc != 3)
    {
        fprintf(stderr, "Usage: %s send_file\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // 第 1 步：创建监听套接字  
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1)
    {
        perror("socket error");
        exit(EXIT_FAILURE);
    }

    // 第 2 步：绑定
    struct sockaddr_in myaddr;            // 通过 myaddr 结构体指定要绑定的地址
    myaddr.sin_family = AF_INET;          // 指定地址家族为 AF_INET
    //myaddr.sin_addr.s_addr = inet_addr("192.168.0.118");  // 指定 IP 地址为本机的某个特定 IP 地址
    myaddr.sin_addr.s_addr = inet_addr(argv[1]);  // 指定 IP 地址为本机任意 IP 地址（0.0.0.0）
    myaddr.sin_port = htons(atoi(argv[2]));          // 指定端口号为 8888

    if(-1 == connect(sock, (struct sockaddr*)&myaddr, sizeof(myaddr)))
    {
        perror("connect error");
        exit(EXIT_FAILURE);
    }

    int ret;

    if(sizeof(file_name) != read(sock, file_name, sizeof(file_name)))
    {
        fprintf(stderr, "read error");
        close(sock);
        exit(EXIT_FAILURE);
    }

    fd = open(file_name, O_WRONLY | O_CREAT);
    if(fd == -1)
    {
        perror("open error");
        exit(EXIT_FAILURE);
    }

    char msg1[1000];
    char msg2[1000];
    char msg3[1000];
    while((ret = read(sock, buff, sizeof(buff))) > 0)
    {
        if(write(fd, buff, ret) != ret) break;
    }

    read(sock, msg1, sizeof(msg1));
    read(sock, msg2, sizeof(msg2));

    printf("ms1:%s\nms2:%s\n", msg1, msg2);

    // 第 7 步：关闭套接字
    close(sock);
    close(fd);

    return 0;
}

