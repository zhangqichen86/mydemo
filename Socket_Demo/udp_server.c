#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>


int main()
{
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd == -1)
    {
        perror("socket error");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in myaddr;

    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = INADDR_ANY;
    myaddr.sin_port = htons(8888);

    if(-1 == bind(sockfd, (struct sockaddr*)&myaddr, sizeof(myaddr)))
    {
        perror("bind error");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in target_addr;
    target_addr.sin_family = AF_INET;
    target_addr.sin_addr.s_addr = inet_addr("192.168.0.118");
    target_addr.sin_port = htons(8888);

    char msg[500];
    int ret;

    ret = recv(sockfd, msg, sizeof(msg) - 1, 0);

    if(ret > 0)
    {
        msg[ret] = '\0';
        printf("%s\n", msg);
    }

    strcpy(msg, "我是张哥");

    struct sockaddr_in target_addr;

    target_addr.sin_family = AF_INET;
    target_addr.sin_addr.s_addr = inet_addr("192.168.0.118");
    target_addr.sin_port = htons(8888);

    sendto(sockfd, msg, strlen(msg), 0, (struct sockaddr*)&target_addr, sizeof(target_addr));




    return 0;
}