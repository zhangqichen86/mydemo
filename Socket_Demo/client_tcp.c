#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>       
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>


int main(int argc, char** argv)
{
    // 第 1 步：创建套接字  
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    if(sock == -1)
    {
        perror("socket error");
        exit(EXIT_FAILURE);
    }

    /*
    // 第 2 步（可选）：绑定
    struct sockaddr_in myaddr;            // 通过 myaddr 结构体指定要绑定的地址
    myaddr.sin_family = AF_INET;          // 指定地址家族为 AF_INET
    //myaddr.sin_addr.s_addr = inet_addr("192.168.0.118");  // 指定 IP 地址为本机的某个特定 IP 地址
    myaddr.sin_addr.s_addr = INADDR_ANY;  // 指定 IP 地址为本机任意 IP 地址（0.0.0.0）
    myaddr.sin_port = htons(99);          // 指定端口号为 99

    if(-1 == bind(sock_listen, (struct sockaddr*)&myaddr, sizeof(myaddr)))
    {
        perror("bind error");
        exit(EXIT_FAILURE);
    }
    */

    // 第 3 步：连接服务器

    // 指定目标服务器的地址
    struct sockaddr_in srv_addr;
    srv_addr.sin_family = AF_INET;
    srv_addr.sin_addr.s_addr = inet_addr(argv[1]);  // 服务器 IP
    srv_addr.sin_port = htons(atoi(argv[2]));       // 服务器端口号

    // 连接目标服务器
    if(-1 == connect(sock, (struct sockaddr*)&srv_addr, sizeof(srv_addr)))
    {
        perror("cpnnect error");
        exit(EXIT_FAILURE);
    }

    // 第 4 步：收发数据
    char msg[1025];
    int ret;

    while(1)
    {
        printf("\n我  ：");
        scanf("%1024[^\n]%*c", msg);

        send(sock, msg, strlen(msg), 0);               // 发送数据

        if(strcmp(msg, "q") == 0) break;

        ret = recv(sock, msg, sizeof(msg) - 1, 0);    // 接收数据

        if(ret > 0)
        {
            msg[ret] = '\0';
            printf("\n客服：%s\n", msg);
        }
    }

    // 第 5 步：断开连接
    close(sock);

    return 0;
}