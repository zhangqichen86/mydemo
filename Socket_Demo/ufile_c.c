#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>


#pragma pack(push, 1)

typedef struct files
{
    int io_where;
    int cnt;
    char data[492];
} files;

#pragma pack(pop)

int main(int argc, char** argv)
{
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd == -1)
    {
        perror("socket error");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in myaddr;

    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    myaddr.sin_port = htons(6666);

    if(-1 == bind(sockfd, (struct sockaddr*)&myaddr, sizeof(myaddr)))
    {
        perror("bind error");
        exit(EXIT_FAILURE);
    }

    files target_file;

    char file_name[100];
    char target_name[100];
    int cnt = 0;

    struct sockaddr_in target_addr;            // 通过 myaddr 结构体指定要绑定的地址
    target_addr.sin_family = AF_INET;          // 指定地址家族为 AF_INET
    target_addr.sin_addr.s_addr = inet_addr("127.0.0.1");  // 指定 IP 地址为本机的某个特定 IP 地址
    target_addr.sin_port = htons(8888);        // 指定端口号为 8888    

    connect(sockfd, (struct sockaddr*)&target_addr, sizeof(target_addr));
    char is_ok[10] = "ok";
    send(sockfd, is_ok, sizeof(is_ok), 0);
    

    cnt = recv(sockfd, file_name, sizeof(file_name), 0);
    if(cnt > 0)
    {
        file_name[cnt] = '\0';
    }
    strcpy(target_name, file_name);

   
    int fd = open(target_name, O_WRONLY | O_CREAT, 0640);
    if(-1 == fd)
    {
        perror("open error");
        exit(EXIT_FAILURE);
    }

    int ret;
    
    while(1)
    {
        ret = recv(sockfd, &target_file, sizeof(target_file), 0);
        
        if(target_file.io_where == -1)
            break;

        if(ret >= 0)
        {
            lseek(fd, target_file.io_where, SEEK_SET);
            write(fd, target_file.data, target_file.cnt);
        }
    }

    printf("拷贝文件完成！\n");

    close(fd);
    close(sockfd);

    return 0;
}