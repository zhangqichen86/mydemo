#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>


void* send_thr(void* arg);


int main(int argc, char** argv)
{
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd == -1)
    {
        perror("socket error");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in myaddr;

    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    myaddr.sin_port = htons(6666);

    if(-1 == bind(sockfd, (struct sockaddr*)&myaddr, sizeof(myaddr)))
    {
        perror("bind error");
        exit(EXIT_FAILURE);
    }

    int tmp = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &tmp, sizeof(tmp));

    struct sockaddr_in target_addr;
    target_addr.sin_family = AF_INET;
    target_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    target_addr.sin_port = htons(8888);

    char msg[500];
    int ret;

    connect(sockfd, (struct sockaddr*)&target_addr, sizeof(target_addr));

    pthread_t tid;

    pthread_create(&tid, NULL, send_thr, (void*)(long)sockfd);

    while(1)
    {
        printf("\n我说:");
        
        fflush(stdout);

        scanf("%499[^\n]%*c", msg);

        sendto(sockfd, msg, strlen(msg), 0, (struct sockaddr*)&target_addr, sizeof(target_addr));
    }

    close(sockfd);

    return 0;
}


void* send_thr(void* arg)
{
    int sockfd = (long)arg;
    char msg[500];
    int ret;

    struct sockaddr_in sender_addr;
    socklen_t addr_len;
    addr_len = sizeof(sender_addr);

    pthread_detach(pthread_self());

    while(1)
    {
        addr_len = sizeof(sender_addr);

        ret = recvfrom(sockfd, msg, sizeof(msg) - 1, 0, (struct sockaddr*)&sender_addr, &addr_len);

        if(ret > 0)
        {
            msg[ret] = '\0';
            printf("\r用户%s:%s\n\n我说:", inet_ntoa(sender_addr.sin_addr), msg);
            fflush(stdout);
        }
    }
   
    // sendto(sockfd, msg, strlen(msg), 0, (struct sockaddr*)&target_addr, sizeof(target_addr));


    return NULL;
}
