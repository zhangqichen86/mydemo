#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>


#pragma pack(push, 1)

typedef struct files
{
    int io_where;
    int cnt;
    char data[492];
} files;

#pragma pack(pop)

int main(int argc, char** argv)
{
    const char* p = NULL;
    const char* send_file_path = NULL;
    char file_name[100] = "";

    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd == -1)
    {
        perror("socket error");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in myaddr;

    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    myaddr.sin_port = htons(8888);

    if(-1 == bind(sockfd, (struct sockaddr*)&myaddr, sizeof(myaddr)))
    {
        perror("bind error");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in target_addr;            // 通过 myaddr 结构体指定要绑定的地址
    target_addr.sin_family = AF_INET;          // 指定地址家族为 AF_INET
    target_addr.sin_addr.s_addr = inet_addr("127.0.0.1");  // 指定 IP 地址为本机的某个特定 IP 地址
    target_addr.sin_port = htons(6666);        // 指定端口号为 8888    

    connect(sockfd, (struct sockaddr*)&target_addr, sizeof(target_addr));

    char is_ok[10] = "";
    recv(sockfd, is_ok, sizeof(is_ok), 0);
    if(strncmp(is_ok, "ok", 2) != 0)
    {
        printf("非法访问！\n");
        exit(0);
    }

    send_file_path = argv[1];

    p = strrchr(send_file_path, '/');

    if(p != NULL) p++;

    else p = send_file_path;

    strcpy(file_name, p);

    send(sockfd, file_name, strlen(file_name), 0);

    int fd = open(send_file_path, O_RDONLY);
    if(-1 == fd)
    {
        perror("open error");
        exit(EXIT_FAILURE);
    }

    files source_file;

    source_file.io_where = 0;       // 位置初始化
    int ret = 1;
    
    while(ret > 0)
    {
        ret = read(fd, source_file.data, sizeof(source_file.data));
        source_file.cnt = ret;
        
        send(sockfd, &source_file, sizeof(source_file), 0);
        source_file.io_where += sizeof(source_file.data);
        usleep(1000);
        if(ret < sizeof(source_file.data))
        {
            usleep(10000);
            source_file.io_where = -1;      // 文件读操作完成，将位置置为 -1 作为结束标志
            send(sockfd, &source_file, sizeof(source_file), 0);
            break;
        }
    }

    printf("传输文件完成！\n");

    close(fd);
    close(sockfd);

    return 0;
}
